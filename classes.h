#ifndef INCLUDED_H_CLASSES
#define INCLUDED_H_CLASSES

#define LENGTH_MAX 255
#define JS_LINES 12
#define PHP_LINES 38

int write_into_file(FILE*, char*, int, const char[][LENGTH_MAX]);
int create_class_js(FILE*, char*);
int create_class_php(FILE*, char*);
int create_class_file(char*, int, char*,bool);

#endif