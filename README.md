# New-PhpGenClass

New-PhpGenClass is a tool for [SQL Maestro PHP Generator](https://www.sqlmaestro.com/products/postgresql/phpgenerator/about/). It generates template files for server and client events.

## How to compile ?

New-PhpGenClass is a command line Windows application. It has been writed in C and compiled with the GNU C compiler version 8.3.0.

To build the executable file, just type `make`.

## How to use ?

The following example generates a PHP Class named 'MyClass'. The file 'MyClass.php' is created in the 'classes' subfolder.

    New-PhpGenClass -c server -d .\classes\ MyClass

## Options

* `-c classType` : Provide the class type. Must be string. Accepted values are 'server' (PHP class) and 'client' (Javascript class),
* `-d folder` : Provide the destination folder. Must be string. If unspecified, the current folder is used,
* `-f` : The previous class file is overwritted,
* `-h` : Print a short help statement,
* `-v` : Display version.

## Licence

Copyright 2022 COPYRIGHT Olivier Auverlot

This program is free software; you can redistribute it and/or modify it under the terms of the MIT License.
