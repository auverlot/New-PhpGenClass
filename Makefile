CC = gcc
CFLAGS = -O3 -MMD
EXE_NAME = New-PhpGenClass
LDFLAGS = 

src = $(wildcard *.c)
objs = $(src:.c=.o)
dep = $(objs:.o=.d)  # one dependency file for each source	

$(EXE_NAME): $(objs)
	$(CC) -o $(@) $^ $(LD_FLAGS)

-include $(dep)   # include all dep files in the makefile

.PHONY: clean
clean:
	rm -f *.o $(EXE_NAME)

.PHONY: cleandep
cleandep:
	rm -f $(dep)
