#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "classes.h"
#include "genclass.h"

const char template_js[JS_LINES][LENGTH_MAX] = {
    "class %s {\n",
        "\tconstructor() { }\n\n",
        "\tonBeforePageLoad() { }\n\n",
        "\tonAfterPageLoad() { }\n\n",
        "\tonInsertFormLoaded(editors) { }\n\n",
        "\tonEditFormLoaded(editors) { }\n\n",
        "\tonInsertFormEditorValueChanged(sender,editors) { }\n\n",
        "\tonEditFormEditorValueChanged(sender,editors) { }\n\n",
        "\tonInsertFormValidate(fieldValues,errorInfo) { }\n\n",
        "\tonEditFormValidate(fieldValues,errorInfo) { }\n\n",
        "\tonCalculateControlValues(editors) { }\n",
    "}\n"
};

const char template_php[PHP_LINES][LENGTH_MAX] = {
    "<?php\n",
        "class %s {\n",                                                                                                                   
        "\tpublic function __construct() { }\n\n",
        "\tpublic function __destruct() { }\n\n",
        "\tprivate function setFieldContent($fieldName,$fieldData,&$customText,&$handled) { }\n\n",
        "\tpublic function onBeforePageExecute() { }\n\n",
        "\tpublic function onPreparePage() { }\n\n",
        "\tpublic function onGetCustomPagePermissions($pageName,$userId,$userName,$connection,&$permissions) { }\n\n",
        "\tpublic function onGetCustomRecordPermissions($page,&$usingCondition,$rowData,&$allowEdit,&$allowDelete,&$mergeWithDefault,&$handled) { }\n\n",
        "\tpublic function onAddEnvironmentVariables($page,&$variables) { }\n\n",
        "\tpublic function onPageLoaded() { }\n\n",
        "\tpublic function onPrepareColumnFilter($columnFilter) { }\n\n",
        "\tpublic function onPrepareFilterBuilder($filterBuilder,$columns) { }\n\n",
        "\tpublic function onGetSelectionFilters($columns,&$result) { }\n\n",
        "\tpublic function onGetCustomFormLayout($mode,$columns,$layout) { }\n\n",
        "\tpublic function onGetCustomColumnGroup($columns,$columnGroup) { }\n\n",
        "\tpublic function onCustomCompareValues($columnName,$valueA,$valueB,&$result) { }\n\n",
        "\tpublic function onFileUpload($fieldName,$rowData,&$result,&$accept,$originalFileName,$originalFileExtension,$fileSize,$tempFileName) { }\n\n",
        "\tpublic function onGetCustomExportOptions($page,$exportType,$rowData,&$options) { }\n\n",
        "\tpublic function onCustomHTMLHeader($page,&$customHtmlHeaderText) { }\n\n",
        "\tpublic function onGetCustomTemplate($type,$part,$mode,&$result,&$params) { }\n\n",
        "\tpublic function onCustomRenderColumn($fieldName,$fieldData,$rowData,&$customText,&$handled) { }\n\n",
        "\tpublic function onCustomRenderPrintColumn($fieldName,$fieldData,$rowData,&$customText,&$handled) { }\n\n",
        "\tpublic function onCustomRenderExportColumn($exportType,$fieldName,$fieldData,$rowData,&$customText,&$handled) { }\n\n",
        "\tpublic function onCustomDrawRow($rowData,&$cellFontColor,&$cellFontSize,&$cellBgColor,&$cellItalicAttr,&$cellBoldAttr) { }\n\n",
        "\tpublic function onExtendedCustomDrawRow($rowData,&$rowCellStyles,&$rowStyles,&$rowClasses,&$cellClasses) { }\n\n",
        "\tpublic function onCustomRenderTotals($totalValue,$aggregate,$columnName,&$customText,&$handled) { }\n\n",
        "\tpublic function onCustomDefaultValues(&$values,&$handled) { }\n\n",
        "\tpublic function onCalculateFields($rowData,$fieldName,&$value) { }\n\n",
        "\tpublic function onGetFieldValue($fieldName,&$value,$tableName) { }\n\n",
        "\tpublic function onBeforeInsertRecord($page,&$rowData,$tableName,&$cancel,&$message,&$messageDisplayTime) { }\n\n",
        "\tpublic function onBeforeUpdateRecord($page,$oldRowData,&$rowData,$tableName,&$cancel,&$message,&$messageDisplayTime) { }\n\n",
        "\tpublic function onBeforeDeleteRecord($page,&$rowData,$tableName,&$cancel,&$message,&$messageDisplayTime) { }\n\n",
        "\tpublic function onAfterInsertRecord($page,$rowData,$tableName,&$success,&$message,&$messageDisplayTime) { }\n\n",
        "\tpublic function onAfterUpdateRecord($page,$oldRowData,$rowData,$tableName,&$success,&$message,&$messageDisplayTime) { }\n\n",
        "\tpublic function onAfterDeleteRecord($page,$rowData,$tableName,&$success,&$message,&$messageDisplayTime) { }\n",
        "}\n",
    "?>\n"
};

int write_into_file(FILE* fd, char* classname, int nbr_lines, const char template[][LENGTH_MAX]) {
    int ret;
    for(int i=0; i<nbr_lines; i++) {
        ret = fprintf(fd, template[i],classname);
        if(ret < 0) {
            fprintf (stderr, "Write error during class creation.\n");
            exit(1);
        }
    }

    return true;
}

int create_class_js(FILE* fd, char* classname) {
    return write_into_file(fd, classname, JS_LINES, template_js);
}

int create_class_php(FILE* fd, char* classname) {
    return write_into_file(fd, classname, PHP_LINES, template_php);
}

int create_class_file(char* classname, int classtype, char* dest,bool force) {
    FILE* fd;
    bool err;
    char filepath[512];
    char *extension;
    char key;

    // The file extension is based on the class type
    extension = (classtype == CLIENT_CLASS) ? ".js" : ".php";

    sprintf(filepath, "%s%s%s", dest, classname, extension);

    if( access( filepath, F_OK ) == 0 && force == false) {
        do {
            printf("The %s file already exists. Do you want overwrite it (Yes/No) ? ",filepath);
            scanf("%c",&key);
        } while(key != 'Y' && key != 'y' && key != 'N' && key != 'n');
        if(key == 'N' || key == 'n') {
            exit(0);
        } 
    }

    fd = fopen( filepath, "w" );
    
    if(fd != NULL) {
        switch(classtype) {
            case CLIENT_CLASS:
                err = create_class_js(fd,classname);
                break;
            case SERVER_CLASS: 
                err = create_class_php(fd,classname);
                break;
            default:
                fprintf (stderr, "The class type is not supported.\n");
                exit(1);
        }
        
        fclose(fd);
    } else {
        perror("Error during class file creation: ");
        exit(1);
    }

    return 0;
}
