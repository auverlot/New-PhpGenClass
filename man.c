#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "man.h"

void display_banner(bool quit) {
    printf("New-PhpGenClass version %s\n",VERSION);
    if(quit) {
        exit(0);
    }
}

void display_man(int errcode) {
    display_banner(false);
    puts("");
    puts("SYNOPSIS\n");
    puts("\tNew-PhpGenClass [OBJECT] className\n");
    puts("DESCRIPTION\n");
    puts("\tNew-PhpGenClass is a tool for SQL Maestro PHP Generator. It generates template files for server and client events.\n");
    puts("OBJECT\n");
    puts("\t-c classType");
    puts("\t\tProvide the class type. Must be string. Accepted values are 'server' (PHP class) and 'client' (Javascript class).");
    puts("");
    puts("OPTIONS\n");
    puts("\t-d folder");
    puts("\t\tProvide the destination folder. Must be string. If unspecified, the current folder is used.");
    puts("\t-f");
    puts("\t\tThe previous class file is overwritted.");
    puts("\t-h");
    puts("\t\tPrint a short help statement.");
    puts("\t-v");
    puts("\t\tDisplay version.");
    puts("");
    puts("EXAMPLE\n");
    puts("\tNew-PhpGenClass -c server -d .\\classes\\ MyClass");
    puts("\t\tGenerates a PHP Class named 'MyClass'. The file 'MyClass.php' is created in the 'classes' subfolder.");
    puts("");
    puts("LICENCE\n");
    puts("This program is free software; you can redistribute it and/or modify it under the terms of the MIT License.\n");
    puts("Copyright 2022 COPYRIGHT Olivier Auverlot\n");
    puts("Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n");
    puts("THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n");
    exit(errcode);
}