#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>

#include "genclass.h"
#include "man.h"
#include "classes.h"

/*
    New-PhpGenClass -c server -d ./ myclass
*/
int main(int argc, char **argv) {
    int c;
    int index;
    char *class_name = "";
    char *dest_folder = "./";
    opterr = 0;
    int class_type;
    bool force = false;

    while ((c = getopt (argc, argv, "c:d:fhv")) != -1) {
        switch(c) {
            case 'c':
                class_name = strlwr(optarg);
                if(strcmp(class_name,"server") == 0) {
                    class_type = SERVER_CLASS;
                } else {
                    if(strcmp(class_name,"client") == 0) {
                        class_type = CLIENT_CLASS;
                    } else {
                        fprintf (stderr, "The class type \"%s\" is not supported.\n", class_name);
                        return 1;
                    }
                }
                break;
            case 'd':
                dest_folder = optarg;
                DIR* dir = opendir(dest_folder);
                if (!dir) {
                    fprintf (stderr, "The directory \"%s\" can't be used.\n", dest_folder);
                } 
                break;
            case 'f': 
                force = true;
                break;
            case 'h':
                display_man(0);
                break;
            case 'v':
                display_banner(true);
                break;
            case '?':
                if (optopt == 'c' || optopt == 'd') {
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                    return 1;
                }
                break;
        }
    }

    if(optind < argc) {
        int ret = create_class_file(argv[optind],class_type,dest_folder,force);
    } else {
        puts("You must specified the name of the class.");
        return 1;
    }
   
    return 0;
}