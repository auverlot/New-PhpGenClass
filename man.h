#ifndef INCLUDED_H_MAN
#define INCLUDED_H_MAN

#define VERSION "1.1"

void display_banner(bool);
void display_man(int);

#endif